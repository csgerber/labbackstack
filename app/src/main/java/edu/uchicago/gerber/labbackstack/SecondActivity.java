package edu.uchicago.gerber.labbackstack;

import android.content.Intent;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.view.View;
import android.view.ViewManager;
import android.widget.Button;

import static android.content.Intent.FLAG_ACTIVITY_CLEAR_TASK;
import static android.content.Intent.FLAG_ACTIVITY_CLEAR_TOP;

public class SecondActivity extends AppCompatActivity implements View.OnClickListener {

    private Button btnMain, btnSecond, btnThird;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_second);


        btnMain = findViewById(R.id.btnMain);
        btnSecond = findViewById(R.id.btnSecond);
        btnThird = findViewById(R.id.btnThird);

       // View.OnClickListener myListener = ButtonRouter.getInstance(getApplicationContext());

        btnMain.setOnClickListener(this);
        btnSecond.setOnClickListener(this);
        btnThird.setOnClickListener(this);
    }

    @Override
    public void onClick(View view) {



        if (!(view instanceof Button)){
            return;
        }
        Button button  = (Button) view;
        String name = button.getText().toString();
        Intent intent = null;


        switch (name) {
            case "Main":
                intent = new Intent(this, MainActivity.class);
                break;
            case "Second":
                intent = new Intent(this, SecondActivity.class);

                break;
            case "Third":
                intent = new Intent(this, ThirdActivity.class);

                break;
            default:
                break;
        }
        intent.addFlags(Intent.FLAG_ACTIVITY_NEW_TASK | Intent.FLAG_ACTIVITY_CLEAR_TASK);
        startActivity(intent);
        finish();



    }
}
