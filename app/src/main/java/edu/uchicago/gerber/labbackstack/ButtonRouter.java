package edu.uchicago.gerber.labbackstack;

import android.content.Context;
import android.content.Intent;
import android.view.View;
import android.widget.Button;

import static android.content.Intent.FLAG_ACTIVITY_CLEAR_TOP;
import static android.content.Intent.FLAG_ACTIVITY_SINGLE_TOP;

public class ButtonRouter implements View.OnClickListener {

    private static Context context;
    private static  ButtonRouter buttonRouter = null;

    //make this private
    private ButtonRouter(Context context){
        this.context = context;
    }

    //you MUST pass the application context in here
    public static ButtonRouter getInstance(Context appContext){

        if (buttonRouter == null){
            buttonRouter = new ButtonRouter(appContext);
        }
        return  buttonRouter;

    }


    @Override
    public void onClick(View view) {

        if (!(view instanceof Button)){
            return;
        }
        Button button  = (Button) view;
        String name = button.getText().toString();
        Intent intent = null;


        switch (name) {
            case "Main":
               intent = new Intent(context, MainActivity.class);
                break;
            case "Second":
               intent = new Intent(context, SecondActivity.class);

                break;
            case "Third":
                intent = new Intent(context, ThirdActivity.class);

                break;
             default:
                 break;
        }
        intent.addFlags(FLAG_ACTIVITY_CLEAR_TOP);
        context.startActivity(intent);

    }
}
